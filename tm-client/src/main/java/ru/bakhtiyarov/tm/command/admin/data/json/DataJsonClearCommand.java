package ru.bakhtiyarov.tm.command.admin.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

public final class DataJsonClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear json file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON CLEAR]");
        @NotNull SessionDTO session = serviceLocator.getSessionService().getSession();
        endpointLocator.getAdminDataEndpoint().clearJson(session);
        System.out.println("[OK]");
    }

}