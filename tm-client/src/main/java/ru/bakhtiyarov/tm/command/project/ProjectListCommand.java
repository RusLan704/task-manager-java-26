package ru.bakhtiyarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.ProjectDTO;
import ru.bakhtiyarov.tm.endpoint.ProjectEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @NotNull SessionDTO session = serviceLocator.getSessionService().getSession();
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsBySession(session);
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
        }
        System.out.println("[OK]");
    }

}