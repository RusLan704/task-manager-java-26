package ru.bakhtiyarov.tm.api.locator;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.endpoint.*;

public interface EndpointLocator {

    @NotNull
    AdminDataEndpoint getAdminDataEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

}
