package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.IUserEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.api.service.converter.IConverterLocator;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.dto.UserDTO;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO createUserByLoginPassword(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable User user = serviceLocator.getUserService().create(login, password);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO createUserByLoginPasswordEmail(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable User user = serviceLocator.getUserService().create(login, password, email);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO createUserByLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final Role role
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, role);
        @Nullable User user = serviceLocator.getUserService().create(login, password, role);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable User user = serviceLocator.getUserService().findByLogin(login);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findUserById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable User user = serviceLocator.getUserService().findById(session.getUserId());
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserPassword(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable User user = serviceLocator.getUserService().updatePassword(session.getUserId(), password);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserEmail(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable User user = serviceLocator.getUserService().updateUserEmail(session.getUserId(), email);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "firstName", partName = "firstName") final String firstName
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable User user = serviceLocator.getUserService().updateUserFirstName(session.getUserId(), firstName);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserLastName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "lastName", partName = "lastName") final String lastName
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable User user = serviceLocator.getUserService().updateUserLastName(session.getUserId(), lastName);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserMiddleName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "middleName", partName = "middleName") final String middleName
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable User user = serviceLocator.getUserService().updateUserMiddleName(session.getUserId(), middleName);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUserLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable User user = serviceLocator.getUserService().updateUserLogin(session.getUserId(), login);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

}
