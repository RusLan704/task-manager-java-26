package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.IProjectEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.api.service.converter.IConverterLocator;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    public ProjectEndpoint() {
        super(null);
    }

    @Override
    @SneakyThrows
    public void createProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws AccessDeniedException {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name);
    }

    @Override
    @SneakyThrows
    public void createProjectByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @Override
    @SneakyThrows
    public void removeAllProjectsBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeAll(session.getUserId());
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO findProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Project project = serviceLocator.getProjectService().findOneById(session.getUserId(), id);
        @Nullable ProjectDTO projectDTO = converterLocator.getProjectConverter().toDTO(project);
        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAllProjectsBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable List<Project> projects = serviceLocator.getProjectService().findAll(sessionDTO.getUserId());
        return projects
                .stream()
                .map(converterLocator.getProjectConverter()::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO findProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Project project = serviceLocator.getProjectService().findOneByIndex(session.getUserId(), index);
        @Nullable ProjectDTO projectDTO = converterLocator.getProjectConverter().toDTO(project);
        return projectDTO;
    }

    @Override
    @SneakyThrows
    public @Nullable ProjectDTO findProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Project project = serviceLocator.getProjectService().findOneByName(session.getUserId(), name);
        @Nullable ProjectDTO projectDTO = converterLocator.getProjectConverter().toDTO(project);
        return projectDTO;
    }

    @Override
    @SneakyThrows
    @Nullable
    public ProjectDTO removeProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Project project = serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
        @Nullable ProjectDTO projectDTO = converterLocator.getProjectConverter().toDTO(project);
        return projectDTO;
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO removeProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Project project = serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
        @Nullable ProjectDTO projectDTO = converterLocator.getProjectConverter().toDTO(project);
        return projectDTO;
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO removeProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Project project = serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
        @Nullable ProjectDTO projectDTO = converterLocator.getProjectConverter().toDTO(project);
        return projectDTO;
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Project project = serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
        @Nullable ProjectDTO projectDTO = converterLocator.getProjectConverter().toDTO(project);
        return projectDTO;
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Project project = serviceLocator.getProjectService().updateProjectByIndex(session.getUserId(), index, name, description);
        @Nullable ProjectDTO projectDTO = converterLocator.getProjectConverter().toDTO(project);
        return projectDTO;
    }

}
