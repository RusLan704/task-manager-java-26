package ru.bakhtiyarov.tm.api.service.converter;

import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.entity.Session;

public interface ISessionConverter {

    @Nullable
    SessionDTO toDTO(@Nullable Session session);

    @Nullable
    Session toEntity(@Nullable SessionDTO sessionDTO);

}
